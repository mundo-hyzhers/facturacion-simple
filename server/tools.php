<?php
namespace server;

class Tools {
    public static function res_code($code, $data = []) {
        switch ($code) {
            case 202:
                $code_message="Accepted";
                break;
            case 406:
                $code_message="Not Acceptable";
                break;
            default:
                $code = 500;
                $code_message="Internal Server Error";
                break;
        }

        header('HTTP/1.0 '.$code.' '.$code_message);
        echo json_encode($data);
        die();
    }

    public static function validator_simple ($key, $min, $max, $type) {
        $error = "";

        if (isset($_POST[$key])) {
            $size_text=strlen($_POST[$key]);

            if (!empty(trim($_POST[$key]))) {
                if ($size_text >= $min)
                    if ($size_text <= $max)
                        if ($type === 'number') {
                            if (is_numeric($_POST[$key])) 
                                return $error;
                            else $error = "El tipo de dato no corresponde a lo requerido por el sistema";
                        } else {
                            if (gettype($_POST[$key]) === $type) 
                                return $error;
                            else $error = "El tipo de dato no corresponde a lo requerido por el sistema";
                        }
                    else $error = "El valor máximo de letras para el campo es de $max";
                else $error = "El valor mínimo de letras para el campo es de $min";
            } else $error = "Este campo no puede estar vacio";
        } else $error = "No se logró encontrar el dato del campo";

        return $error;
    }
}
