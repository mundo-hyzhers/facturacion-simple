<?php
namespace server\users;

use config\exceptions\DbException;
use server\Tools;
use server\users\{
    Data as Data_User, 
    CRUD_Delete as Delete_User, 
};

class Controller_Delete {

    public static function init() {
       if (isset($_POST)) {
           if ( count($_POST) > 0 ) {
               self::delete_user();
            } else Tools::res_code(500); 
       } else Tools::res_code(500);
    }

    private static function delete_user() { 
        // TODO: Aqui tendría que ir un validador más en forma
        $error = [];

        $error['id'] = Tools::validator_simple('id', 0, 1000, 'number');

        $validator = false;
        if (
            empty($error['id'])
        ) $validator = true;

        if ($validator) {
            // Guardando los datos del formulario en la clase User
            $user = new Data_User();
            $user -> set_id((int)$_POST['id']);

            if (Delete_User::delete($user)) Tools::res_code(202);
            else throw new DbException('DB-002');
        } else Tools::res_code(406, ["Error" => "No logró pasar la validación del Servidor", "Validate" => $error]);
    }
}
