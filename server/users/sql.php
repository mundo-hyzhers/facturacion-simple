<?php
namespace server\users;

class SQL {
    const read_all='SELECT u.id, u.name, u.surname, u.surname_second, u.gender, u.cel, u.rank as id_rank, r.rank, u.create_at, u.update_at FROM users u INNER JOIN users_rank r ON u.rank=r.id ORDER BY u.id';
    const create = 'INSERT INTO users(name, surname, surname_second, gender, cel, rank, create_at, update_at) VALUES(:name, :surname, :surname_second, :gender, :cel, :rank, NOW(), NOW())';
    const update = 'UPDATE users SET name=:name, surname=:surname, surname_second=:surname_second, gender=:gender, cel=:cel, rank=:rank, update_at=NOW() WHERE id=:id';
    const delete = 'DELETE FROM users WHERE id=:id';
}
