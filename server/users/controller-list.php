<?php
namespace server\users;

use server\Tools;
use server\users\CRUD_Read as Read_User;

class Controller_List {
    public static function init() {
       if (isset($_POST['list'])) {
           if ($_POST['list'] === "all_users") {
               Tools::res_code(202, Read_User::all_users());
                Tools::res_code(406, $_POST);
            } else Tools::res_code(500); 
       } else Tools::res_code(500);
    }
}
