<?php
namespace server\users;

use config\exceptions\DataException;

/**
 * Clase que sirve para validar si un campo es de tipo ENUM
 */
abstract class Enum {
    private static $const_cache_array = NULL;

    public static function getConstants() {
        if (is_null(self::$const_cache_array))
            self::$const_cache_array = [];

        $called_class=get_called_class();

        if (!array_key_exists($called_class, self::$const_cache_array)) {
            $the_class=new \ReflectionClass($called_class);
            self::$const_cache_array[$called_class] = $the_class -> getConstants();
        }

        return self::$const_cache_array[$called_class];
    }


    public static function is_valid($name) {
        $constants = self::getConstants();

        return array_key_exists($name, $constants);
    }
}

/**
 * Clase que contiene el ENUM de gender
 */
abstract class Gender extends Enum {
    const male = NULL;
    const female = NULL;
}

class Data {
    private $id = NULL;
    private $name = NULL;
    private $surname = NULL;
    private $surname_second = NULL;
    private $gender = NULL;
    private $cel = NULL;
    private $rank = NULL;
    // TODO: Este campo debería de ir en otra clase aparte
    private $rank_data = NULL;
    private $create_at = NULL;
    private $update_at = NULL;

    public function __construct(){}

    //public function __construct($id = NULL, $name = NULL, $surname = NULL, $surname_second = NULL, $gender = NULL, $cel = NULL, $rank = NULL, $create_at = NULL, $update_at = NULL)
    //{
    //    $this -> id = $id; 
    //    $this -> name = $name;
    //    $this -> surname = $surname;
    //    $this -> surname_second = $surname_second;
    //    $this -> gender = $gender;
    //    $this -> cel = $cel;
    //    $this -> rank = $rank;
    //    $this -> create_at = $create_at;
    //    $this -> update_at = $update_at;
    //}

    private function is_valid($type, $data) {
        if (gettype($data) === $type)
            return true;
        
        return false;
    }

    // id
    public function get_id() {
        return $this -> id;
    }
    public function set_id($id) {
        if ($this -> is_valid("integer", $id))
            $this -> id = $id;
        else throw new DataException("DATA-001", "<b>".__FUNCTION__."</b> | Solo se permiten valores Integers");
    }

    // name
    public function get_name() {
        return $this -> name;
    }
    public function set_name($name) {
        if ($this -> is_valid("string", $name))
            $this -> name = $name;
        else throw new DataException("DATA-001", "<b>".__FUNCTION__."</b> | Solo se permiten valores String");
    }

    // surname
    public function get_surname() {
        return $this -> surname;
    }
    public function set_surname($surname) {
        if ($this -> is_valid("string", $surname))
            $this -> surname = $surname;
        else throw new DataException("DATA-001", "<b>".__FUNCTION__."</b> | Solo se permiten valores String");
    }

    // surname_second
    public function get_surname_second() {
        return $this -> surname_second;
    }
    public function set_surname_second($surname_second) {
        if ($this -> is_valid("string", $surname_second))
            $this -> surname_second = $surname_second;
        else throw new DataException("DATA-001", "<b>".__FUNCTION__."</b> | Solo se permiten valores String");
    }

    // gender
    public function get_gender() {
        return $this -> gender;
    }
    public function set_gender($gender) {
        if (Gender::is_valid($gender))
            $this -> gender = $gender;
        else throw new Dataexception("DATA-001", "<b>".__FUNCTION__."</b> | Solo se permiten los valores male - female");
    }


    // cel
    public function get_cel() {
        return $this -> cel;
    }
    public function set_cel($cel) {
        if (is_array($cel))
            $this -> cel = $cel;
        else throw new Dataexception("DATA-001", "<b>".__FUNCTION__."</b> | Solo se permiten valores JSON");
    }

    // rank
    public function get_rank() {
        return $this -> rank;
    }
    public function set_rank($rank) {
        if ($this -> is_valid("integer", $rank))
            $this -> rank = $rank;
        else throw new DataException("DATA-001", "<b>".__FUNCTION__."</b> | Solo se permiten valores Integers");
    }
    // rank_data TODO: Esto debería de ser diferente
    public function get_rank_data() {
        return $this -> rank_data;
    }
    public function set_rank_data($rank) {
        if ($this -> is_valid("string", $rank))
            $this -> rank_data = $rank;
        else throw new DataException("DATA-001", "<b>".__FUNCTION__."</b> | Solo se permiten valores Strings");
    }

    // create_at
    public function get_create_at() {
        return $this -> create_at;
    }
    public function set_create_at($create_at) {
        if ($this -> is_valid("string", $create_at))
            $this -> create_at = $create_at;
        else throw new DataException("DATA-001", "<b>".__FUNCTION__."</b> | Solo se permiten valores String");
    }

    // update_at
    public function get_update_at() {
        return $this -> update_at;
    }
    public function set_update_at($update_at) {
        if ($this -> is_valid("string", $update_at))
            $this -> update_at = $update_at;
        else throw new DataException("DATA-001", $update_at, "<b>".__FUNCTION__."</b> | Solo se permiten valores String");
    }
}
