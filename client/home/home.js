/**
 * Clase obtenedora de los elementos del browser
 */
class Nodes {
    
    form = null

    form_name = null
    form_surname = null
    form_surname_second = null
    form_gender = null
    form_cel = null
    form_rank = null
    form_send = null
    form_new = null

    error_name = null
    error_surname = null
    error_surname_second = null
    error_gender = null
    error_cel = null
    error_rank = null

    form_notice = null
    
    report = null
    notice_report = null

    table = null
    
    constructor () {
        this.form = document.getElementById("form")

        this.form_name = document.getElementById('name');
        this.form_surname = document.getElementById('surname');
        this.form_surname_second = document.getElementById('surname_second');
        this.form_gender = document.querySelectorAll('input[name="gender"]');
        this.form_cel = document.getElementById('cel');
        this.form_rank = document.getElementById('rank');
        this.form_send = document.getElementById('send');
        this.form_new = document.getElementById('new')

        this.error_name = document.getElementById('error_name');
        this.error_surname = document.getElementById('error_surname');
        this.error_surname_second = document.getElementById('error_surname_second');
        this.error_gender = document.getElementById('error_gender');
        this.error_cel = document.getElementById('error_cel');
        this.error_rank = document.getElementById('error_rank');

        this.form_notice = document.getElementById("notice")

        this.report = document.getElementById("report")
        this.notice_report = document.getElementById("notice_report")

        this.table = document.getElementById("table-content__data")

        this.table_list();
    }

    table_list () {
        const body = new FormData()
        body.append("list", "all_users")

        fetch('controller/users/list', {
            method: "post",
            body
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            this.table.innerHTML="";

            if (data.length) this.report.classList.add("report-show")
            
            data.forEach(row => {
                const html_row = document.createElement("div");
                html_row.classList.add("table-content__row")
                
                Object.keys(row).forEach(key => {
                    if (key == 'id_rank') return;

                    const html_col = document.createElement("div")
                    html_col.classList.add("table-content__col")

                    if (key == 'gender') {
                        if (row[key] == 'male') 
                            html_col.textContent = 'Hombre';
                        else 
                            html_col.textContent = 'Mujer';
                    } else html_col.textContent=row[key]

                    html_row.appendChild(html_col)
                })

                const html_col = document.createElement("div")
                html_col.classList.add("table-content__col")

                const html_edit = document.createElement('span')
                html_edit.classList.add("table-content__col__action")
                html_edit.textContent="Editar"
                html_edit.addEventListener('click', () => {
                    this.set_fields(row)
                })
                html_col.appendChild(html_edit)
                
                const html_delete = document.createElement('span')
                html_delete.classList.add("table-content__col__action")
                html_delete.textContent="Borrar"
                html_delete.addEventListener('click', () => {
                    this.delete_data(row)
                })
                html_col.appendChild(html_delete)

                html_row.appendChild(html_col)

                this.table.appendChild(html_row)
            })
        })
    }

    delete_data (data) {
        const body = new FormData()
        body.append("id", data.id)

        fetch('controller/users/delete', {
            method: "post",
            body
        })
        .then(res => {
            if (res.ok) return "Usuario Eliminado"
            else return res.json()
        })
        .then(data => {
            this.launchNotice("error", data)
                        
            this.clear_form();
            this.table_list();
        })
    }

    set_fields (data) {
        this.form_name.value = data.name
        this.form_surname.value = data.surname
        this.form_surname_second.value = data.surname_second 
        this.form_gender.forEach(radio => { if (radio.value == data.gender) radio.checked = true}) 
        this.form_cel.value = data.cel
        this.form_rank.value = data.id_rank;

        this.form_send.value = data.id
        this.form.send.textContent= "Actualizar"

        this.form_new.classList.add('new-show')
    }

    launchNotice (state, message) {
        this.form_notice.classList.remove("notice-remove")
        this.form_notice.classList.add(`notice-${state}`)
        this.form_notice.textContent = message;

        setTimeout(() => {
            this.form_notice.textContent = "";
            this.form_notice.classList.add("notice-remove");
            setTimeout(() => {
                this.form_notice.classList.remove(`notice-${state}`);
            }, 500)
        }, 10000)
    }

    clear_form () {
        this.form_name.value="";
        this.form_name.focus()
        this.form_surname.value=""
        this.form_surname_second.value=""
        this.form_gender.forEach(gender => gender.checked=false)
        this.form_cel.value=""
        this.form_rank.value=1
    }

    launchNoticeReport (state, message) {
        this.notice_report.classList.remove("notice-remove")
        this.notice_report.classList.add(`notice-${state}`)
        this.notice_report.textContent = message;

        setTimeout(() => {
            this.notice_report.textContent = "";
            this.notice_report.classList.add("notice-remove");
            setTimeout(() => {
                this.notice_report.classList.remove(`notice-${state}`);
            }, 500)
        }, 10000)
    }
}

class Listeners extends Nodes {
    constructor() {
        super() 
       
        this.send();
        this.new_btn();
        this.report_generate()
    }

    new_btn () {
        this.form_new.addEventListener('click', () => {
            this.form_new.classList.remove('new-show')

            this.form_send.value = "none"
            this.form.send.textContent= "Enviar"

            this.clear_form()
        })
    }

    send () {
        this.form_send.addEventListener('click', () => {
            const dataForm = new FormData(this.form)
            
            let url ="controller/users/create" 
            // Buscar una mejor forma de almacenar el ID
            if (this.form_send.value != 'none') {
                dataForm.append("id", this.form_send.value)
                url="controller/users/update"
            }

            // Borrar los Errores
            this.error_name.textContent="";
            this.error_surname.textContent=""
            this.error_surname_second.textContent=""
            this.error_gender.textContent=""
            this.error_cel.textContent=""
            this.error_rank.textContent=""
            
            this.form_notice.classList.remove("notice-remove")
            this.form_notice.classList.remove("notice-success")
            this.form_notice.classList.remove("notice-error")

            fetch(url, {
                method: "post",
                body: dataForm
            })
                .then(res => {
                    if (res.ok) {
                        if (this.form_send.value != 'none') return "Usuario Actualizado";
                        else return "Usuario Agregado"
                    } else return res.json()
                })
                . then(data => {
                    if (typeof data == "string") {
                        this.launchNotice("success", data)
                        
                        this.clear_form();
                        this.table_list();

                        if (this.form_send.value != 'none') {
                            this.form_new.classList.remove('new-show')

                            this.form_send.value = "none"
                            this.form.send.textContent= "Enviar"

                            this.clear_form()
                        }
                    } else if (typeof data == "object") {
                        // Validando el tipo de error obtenido
                        if ("Error" in data)
                            this.launchNotice("error", data['Error'])

                        if ("Validate" in data) {
                            Object.keys(data['Validate']).forEach(key => {
                                if (key == 'id') return

                                eval(`this.error_${key}.textContent = '${data.Validate[key]}'`)
                            })
                        }
                    } else {
                        console.error("Error de la petición");
                    }
                })
        })
    } 

    send_DEPRECATED () {
        this.form_send.addEventListener('click', () => {
            const data_name = this.form_name.value;
            const data_surname = this.form_surname.value;
            const data_surname_second = this.form_surname_second.value;
            let data_gender = null
            this.form_gender.forEach(radio => {
                if (radio.checked)
                    data_gender = radio.value
            })
            const data_cel = this.form_cel.value;
            const data_rank = this.form_rank.value;

            console.log(data_name, data_surname, data_surname_second, data_gender, data_cel, data_rank);
        })
    }

    report_generate() {
        this.report.addEventListener('click', () => {
            const body = new FormData()
            body.append("report", "users")

            this.notice_report.classList.remove("notice-remove")
            this.notice_report.classList.remove("notice-success")
            this.notice_report.classList.remove("notice-error")

            fetch('controller/users/report', {
                method: "post",
                body
            })
            .then(res => {
                if (res.ok) return res.json();
                else return null
            })
            .then(data => {
                if (data != null) {
                    if (typeof data.token) {
                        this.launchNoticeReport("success", "Se ha generado el reporte")
                        window.open(`reporte?TOKEN=${data.token}`, "_blank")
                    } else this.launchNoticeReport("error", "No se pudo generar el reporte")
                } else this.launchNoticeReport("error", "No se pudo generar el reporte")
            })
        })
    }
}
new Listeners();
