<?php
namespace config\exceptions;

class DataException extends \Exception {
    protected $code = NULL;

    public function __construct($code, $mensaje) {    
        $this -> code=$code;

        parent::__construct($mensaje);
    }

    public function __toString() {
        $code = $this -> getCode();
        $message = $this -> getMessage();
        $file = $this -> getFile();
        $line = $this -> getLine();

		  return <<<ERROR
			<div style="
				padding: 5px 10px;
				background-color: #df2d2d;
				margin-bottom: 10px;
				">
				
				<h4 style="
					margin-top: 5px;
					padding-bottom: 5px;
					border-bottom: 1px dashed white;	
					color: white;
					">
					Error Base de Datos [$code]
				</h4>
				
				<div style="
					display: flex;
					flex-direction: column;
					">

					<span style="
						color: white;
						font-size: 11px;
						text-align: right;
						flex: auto;
						">
						$file <b>:File</b>
					</span>

					<span style="
						color: white;
						font-size: 11px;
						text-align: right;
						flex: auto;
						">
						$line <b>:Line</b>
					</span>
				
				</div>
				
				<p style="
					width: 100%;
					color: white;
					font-size: 14px; 
					">
					$message
				</p>
				
			</div>
			ERROR;
    }
}
