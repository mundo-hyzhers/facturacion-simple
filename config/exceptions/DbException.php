<?php
namespace config\exceptions;

class DbException extends \Exception {
    protected $code = NULL;
    private $message_err = NULL;

    public function __construct($code) {    
        $this -> code=$code;
        
        switch ($code) {
            case "DB-001":
                $this -> message_err = "No se pudo satisfacer la conexion con la base de datos";
                break;
            case "DB-002":
                $this -> message_err = "No se pudo crear el registro en la base de datos";
                break;
            case "DB-003":
                $this -> message_err = "No se pudo actualizar el registro en la base de datos";
                break;
            case "DB-004":
                $this -> message_err = "No se pudo eliminar el registro en la base de datos";
                break;
        }

        parent::__construct($this -> message_err);
    }

    public function __toString() {
        $code = $this -> getCode();
        $message = $this -> getMessage();
        $file = $this -> getFile();
        $line = $this -> getLine();

		  return <<<ERROR
			<div style="
				padding: 5px 10px;
				background-color: #df2d2d;
				margin-bottom: 10px;
				">
				
				<h4 style="
					margin-top: 5px;
					padding-bottom: 5px;
					border-bottom: 1px dashed white;	
					color: white;
					">
					Error Base de Datos [$code]
				</h4>
				
				<div style="
					display: flex;
					flex-direction: column;
					">

					<span style="
						color: white;
						font-size: 11px;
						text-align: right;
						flex: auto;
						">
						$file <b>:File</b>
					</span>

					<span style="
						color: white;
						font-size: 11px;
						text-align: right;
						flex: auto;
						">
						$line <b>:Line</b>
					</span>
				
				</div>
				
				<p style="
					width: 100%;
					color: white;
					font-size: 14px; 
					">
					$message
				</p>
				
			</div>
			ERROR;
    }
}
