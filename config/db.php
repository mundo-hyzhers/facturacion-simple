<?php
namespace config;

use config\exceptions\DbException; 

define(__NAMESPACE__.'\DB_HOST', 'localhost');
define(__NAMESPACE__.'\DB_DB', 'bills_simple');
define(__NAMESPACE__.'\DB_USER', 'hakyn');
define(__NAMESPACE__.'\DB_PASS', '');

//use const config\DB_HOST;
//use const config\DB_DB;
//use const config\DB_USER;
//use const config\DB_PASS;

class DB {
    // Realizando una clase singleton
    private static $instance = NULL;

    public static function init () {
        if (is_null(self::$instance))
            self::$instance = new DB();

        return self::$instance;
    }

    public static function destroy() {
        self::$instance = NULL;
    }

    private $conn = NULL;

    private function __construct() {
        $this -> conn = NULL;

        $this -> conn = new \PDO('mysql:host='.DB_HOST.';dbname='.DB_DB, DB_USER, DB_PASS);
        $this -> conn -> setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this -> conn -> exec('SET CHARACTER SET utf8');
    }

    public function conn () {
        if (!is_null($this -> conn))
            return $this -> conn;
        else throw new DbException("DB-001");
    }
}
