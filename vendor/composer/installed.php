<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '445504483301c7ba77743634a8eebe25dc81b88c',
        'name' => 'joaquin/bills_simple',
        'dev' => true,
    ),
    'versions' => array(
        'joaquin/bills_simple' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '445504483301c7ba77743634a8eebe25dc81b88c',
            'dev_requirement' => false,
        ),
        'setasign/fpdf' => array(
            'pretty_version' => '1.8.2',
            'version' => '1.8.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../setasign/fpdf',
            'aliases' => array(),
            'reference' => 'd77904018090c17dc9f3ab6e944679a7a47e710a',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
            'dev_requirement' => false,
        ),
        'twig/twig' => array(
            'pretty_version' => 'v3.3.2',
            'version' => '3.3.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../twig/twig',
            'aliases' => array(),
            'reference' => '21578f00e83d4a82ecfa3d50752b609f13de6790',
            'dev_requirement' => false,
        ),
    ),
);
