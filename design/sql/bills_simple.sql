/*CREATE DFATABASE IF NOT EXISTS bills_simple
    DEFAULT CHARACTER SET utf8;
USE bills_simple*/

    CREATE TABLE IF NOT EXISTS users_rank(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        rank VARCHAR(255) NOT NULL UNIQUE,
        create_at DATE NOT NULL,
        update_at DATE NOT NULL
    );

CREATE TABLE IF NOT EXISTS users (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(25) NOT NULL,
    surname VARCHAR(25) NOT NULL,
    surname_second VARCHAR(25) NOT NULL,
    gender ENUM('male', 'female') NOT NULL,
    cel JSON,
rank INT NOT NULL,
    create_at DATE NOT NULL,
    update_at DATE NOT NULL,
CHECK (JSON_VALID(cel)),
FULLTEXT KEY search(name,surname),
FOREIGN KEY(rank)
    REFERENCES users_rank(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

    CREATE TABLE IF NOT EXISTS products_details(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        unit ENUM('pza','g','kg','tonelada','bulto','bolsa') NOT NULL,
        amount DOUBLE NOT NULL,
        price DOUBLE NOT NULL,
    supplier INT NOT NULL,
        cost DOUBLE NOT NULL,
        create_at DATE NOT NULL,
        update_at DATE NOT NULL,
    FOREIGN KEY(supplier)
        REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
    );

CREATE TABLE IF NOT EXISTS products (
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    code VARCHAR(255) NOT NULL UNIQUE,
    image VARCHAR(255) NOT NULL UNIQUE,
detail INT NOT NULL UNIQUE,
    create_at DATE NOT NULL,
    update_at DATE NOT NULL,
FULLTEXT KEY search(name, code),
FOREIGN KEY(detail)
    REFERENCES products_details(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS bills(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
seller INT NOT NULL,
customer INT NOT NULL,
    total DOUBLE NOT NULL,
    create_at DATE NOT NULL,
    update_at DATE NOT NULL,
FOREIGN KEY(seller)
    REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
FOREIGN KEY(customer)
    REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS sales(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
product INT NOT NULL,
    amount DOUBLE NOT NULL,
bill INT NOT NULL,
    create_at DATE NOT NULL,
    update_at DATE NOT NULL,
FOREIGN KEY(product)
    REFERENCES products(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
FOREIGN KEY(bill)
    REFERENCES bills(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);
