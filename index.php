<?php
namespace index;

require_once __DIR__.'/vendor/autoload.php';

require_once __DIR__.'/config/exceptions/DbException.php';
require_once __DIR__.'/config/exceptions/DataException.php';

require_once __DIR__.'/config/db.php';

require_once __DIR__.'/server/tools.php';
require_once __DIR__.'/server/users/controller-list.php';
require_once __DIR__.'/server/users/controller-create.php';
require_once __DIR__.'/server/users/controller-update.php';
require_once __DIR__.'/server/users/controller-delete.php';
require_once __DIR__.'/server/users/controller-report.php';

require_once __DIR__.'/server/users/sql.php';
require_once __DIR__.'/server/users/data.php';
require_once __DIR__.'/server/users/crud.php';

use config\exceptions\DataException;
use config\exceptions\DbException; 
use server\users\{
    Controller_List as Controller_User_List, 
    Controller_Create as Controller_User_Create, 
    Controller_Update as Controller_User_Update,
    Controller_Delete as Controller_User_Delete,
    Controller_Report as Controller_User_Report,
};

define(__NAMESPACE__.'\HOST', 'http://www.facturacion-simple.jrs');
define(__NAMESPACE__.'\CLIENT', __DIR__.'/client');

try {
    if (!isset($_GET['url'])) {
        //$ch = curl_init(HOST."/inicio");
        //curl_setopt($ch, CURLOPT_HEADER, 0);
        //curl_exec($ch);
        //curl_close($ch);
        //die();
        header('Location: /inicio');
        die();
    } else {
        //echo $_GET['url'];
        $url_explode=explode("/", $_GET['url']);
        //var_dump($url_explode);
        if (count($url_explode) == 1) {
            // Aqui se van a renderizar las páginas
            switch ($_GET['url']) {
                case "inicio":
                    $template="home";
                    $twig_dir="home";
                    break;
                case "error":
                    $template="error";
                    $twig_dir="error";
                    break;
                case "reporte":
                    session_start();

                    $err_session = false;
                    if (isset($_SESSION['token'])) {
                        if (isset($_GET['TOKEN'])) {
                            // Validar TOKEN
                            if (password_verify($_SESSION['token'], $_GET['TOKEN']))
                                Controller_User_Report::init_report();
                            else $err_session = true;
                        } else $err_session = true;
                    } else $err_session = true;

                    session_destroy();

                    if ($err_session) header('Location: /error');
                    die();
                    break;
                default:
                    header('Location: /error');
                    die();
                    break;
            }

            $loader = new \Twig\Loader\FilesystemLoader(CLIENT.'/'.$twig_dir);
    
            $twig = new \Twig\Environment($loader);
    
            echo $twig -> render($template.'.twig', ['nombre' => 'Joaquin Reyes Sanchez']);
        } elseif (count($url_explode) == 3 && $url_explode[0] === "controller") {
            // Aqui se van a trabajar los controladores
            switch($url_explode[1]) {
                case "users":
                    switch ($url_explode[2]) {
                        case "list":
                            Controller_User_List::init();
                            break;
                        case "create":
                            Controller_User_Create::init();
                            break;
                        case "update":
                            Controller_User_Update::init(); 
                            break;
                        case "delete":
                            Controller_User_Delete::init();
                            break;
                        case "report":
                            Controller_User_Report::init();           
                            break;
                        default:
                            header('HTTP/1.1 500 Internal Server Error');
                            die();
                            break;
                    }
                    break;
                default:
                    header('HTTP/1.1 500 Internal Server Error');
                    die();
                    break;
            }
        } else {
            header('Location: /error');
            die();
        }
    };
} catch (DbException $e) {
    header('HTTP/1.1 500 Internal Server Error');
    echo json_encode(["Error" => $e -> getMessage(), "Code" => $e -> getCode(), "Line" => $e -> getLine(), "File" => $e -> getFile()]);
    die();
} catch (DataException $e) {
    header('HTTP/1.1 500 Internal Server Error');
    echo json_encode(["Error" => $e -> getMessage(), "Code" => $e -> getCode(), "Line" => $e -> getLine(), "File" => $e -> getFile()]);
    die();
} catch (\Exception $e) {
    header('HTTP/1.1 500 Internal Server Error');
    echo json_encode(["Error" => $e -> getMessage(), "Code" => $e -> getCode(), "Line" => $e -> getLine(), "File" => $e -> getFile()]);
    die();
}


